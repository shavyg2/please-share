import chokidar from "chokidar";
import {fromEvent} from "rxjs/observable/fromEvent";
import {take} from "rxjs/operators/take";
import {takeUntil} from "rxjs/operators/takeUntil";
import {interval} from "rxjs/observable/interval";
import {merge} from "rxjs/observable/merge";
import { chokidar_settings } from "../src/settings/chokidar";
import { resolve } from "path";



describe("chokidar api",()=>{

    chokidar_settings.awaitWriteFinish.stabilityThreshold=200;

    it("should be able to read a folder",async ()=>{
        let watcher = chokidar.watch("**/*",chokidar_settings);
        let file = await fromEvent(watcher,"add").pipe(take(1)).toPromise()
        watcher.close()
    })



    it("should be able to watch folders that don't exist",async ()=>{
        let fake_folder = resolve("fake");

        let watcher = chokidar.watch(fake_folder,chokidar_settings);
        let event1 = fromEvent(watcher,"add").pipe(take(1))
        let event2 = interval(500);

        await merge(event1,event2).pipe(take(1)).toPromise()
        watcher.close()
    })


})