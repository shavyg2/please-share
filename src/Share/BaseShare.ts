import { Api } from "api-foundation";
import { promisify } from "util";
import { join } from "path";
import glob from "glob";
import { Relative } from "../location/Relative";

export const BaseShare = new Api().define((src:string,ignoreOption:string[]=[])=>{


    function GetSrc(){
        return src;
    }

    async function GetFiles(){
        let files:string[] =  promisify(glob)(join(src,"**/*",),{
            ignore:[...["node_modules",".git",".vscode","package.json","package-lock.json"],...ignoreOption]
        })


        return files.map(file=>{
            return new Relative(GetSrc(),file)
        })
    }
    
    return {
        GetSrc,
        GetFiles
    }
})
