import { Api } from "api-foundation";
import pkg from "pkg-up";
import { BaseShare } from "./BaseShare";
                   

export const Share = new Api().define(()=>{

    

})


export const Shared = BaseShare.use((api)=>{



    async function GetPackageJson(){
        let package_json = await pkg(api.GetSrc())
        return require(package_json);
    }

    async function GetName(){
        let pkg = await GetPackageJson();
        return pkg.name;
    }



    return {
        ...api,
        GetName,
        GetPackageJson
    }

}) 




export const Destination = BaseShare.use(api=>{


    return api;
})