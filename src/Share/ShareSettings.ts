




export type Root = string;
export type PackageJson = {
    name:string,
    version:string
}

export interface SharedModule {

    name:string,
    source:string;

    package:PackageJson


}