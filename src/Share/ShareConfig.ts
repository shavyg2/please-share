import { Api } from "api-foundation";
import pkg from "pkg-up";
import { join } from "path";
import { promisify } from "util";
import { exists } from "fs";





export const ShareConfig = new Api().define((src:string)=>{


    
    
    function GetShareConfigPath(){
        let root = join(src,"share.json")
        return root;
    } 


    async function ConfigExist(){
        return promisify(exists)(await GetShareConfigPath())
    }

    async function GetConfig(){
        if(await ConfigExist()){
            return require(await GetShareConfigPath()).shares;
        }else{
            throw new Error("No share.json exist. "+GetShareConfigPath())
        }
    }


    return {
        GetConfig

    }

})  