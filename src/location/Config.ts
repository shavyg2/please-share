import { Api } from "api-foundation";
import memoizee from "memoizee";
import os from "os";
import { join, dirname } from "path";
import { writeFile, access } from "fs";
import { promisify } from "util";
import mkdir from "mkdirp";
import { Shared } from "../Share/Share";


interface Config{

    /**
     * A list of strings that contains the path to all of the shares
     */
    shares:string[]
}

export const shared_foldername = "shared_modules"

export const ConfigApi = new Api().define(()=>{
    let config = join(os.homedir(),".please-share/config.json");
    return config
}).use(config=>{



    const exist = memoizee(async (file)=>{
        try{
            require(file);
          
            return true;
        }catch{
            return false;
        }
    })


    async function GetConfig(){
        if(await exist(config)){
            return require(config) as Config
        }else{
            return {
                shares:[]
            } as Config;
        }
    }


    async function GetShares(){
        let config  = await GetConfig();
        let shares = config.shares

    }


    async function GetSharesFull(){
        let shares  = (await GetConfig()).shares

        let shared_src = shares.map(share=>{
            return Shared.factory.Create(share)
        })


        return shared_src;

    }


    async function WriteConfig(c:Config){

        let folder = dirname(config)


        let is_real =await exist(folder)
        
        if(!is_real){
            await promisify(mkdir)(folder)
        }
        
        return promisify(writeFile)(config,JSON.stringify(c))
    }



    return {
        GetConfig,
        WriteConfig,
        GetSharesFull
    }
})