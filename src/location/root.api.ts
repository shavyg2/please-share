import { Api } from "api-foundation";
import pkg from "pkg-up"
import { dirname } from "path";




export const RootApi = Api.Singleton(()=>{

    let root:string =  pkg.sync();

    return {
        getRoot(){
            if(!root){
                throw new Error("Can't find Package.json");
            }

            return dirname(root);
        }
    }


})