import { resolve, isAbsolute, join } from "path";




export function FullPath(base:string,path:string){
    return isAbsolute(path)?path:join(base,path);
}

export class Relative{
    constructor(private base:string,private path:string){

    }

    get relativePath(){
        let base = resolve(this.base);
        let file = resolve(this.path);

        return file.replace(base,"");
    }

    get FullPath(){
        return FullPath(this.base,this.path);
    }


    isRelativeFile(relative:Relative){
        return this.relativePath === relative.relativePath
    }


}