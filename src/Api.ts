import 'loud-rejection/register';
import "source-map-support/register";

import {Api} from "api-foundation";
import { ConfigApi } from "./location/Config";
import { resolve } from "path";
import { RootApi } from "./location/root.api";
import { ShareConfig } from "./Share/ShareConfig";
import { Shared } from './Share/Share';
import { PackageJson, SharedModule, Root } from './Share/ShareSettings';
import { LinkShare } from './Link';


export const Application = new Api().define(()=>{

    let configApi = ConfigApi.factory.Create();
    let rootapi = RootApi.factory.Create()
    let shareApi = ShareConfig.factory.Create(rootapi.getRoot())


    configApi.GetConfig

    return {

        async addShare(folder:string){
            let config = await configApi.GetConfig()
            
            config.shares.push(resolve(folder))

            let shares = new Set(config.shares)


            config.shares = Array.from(shares)
            await configApi.WriteConfig(config)
        },


        async StartShare(){
            const shares = (await configApi.GetSharesFull());
            const config:string[] = (await shareApi.GetConfig())
            const root:Root = await rootapi.getRoot();


            const available_shares = await Promise.all(shares.map(async share=>{

                let name = await share.GetName();
                let pkg:PackageJson = await share.GetPackageJson()
                let source = share.GetSrc()
                return {
                    name,
                    package:pkg,
                    source
                } as SharedModule
            }))


            const filtered_shares = available_shares.filter(share=>{
                return config.includes(share.name)
            })


            filtered_shares.forEach(share=>{
                LinkShare(share,root);
            })
            
        },


        async GetShares(){

            return configApi.GetSharesFull()
        }

    }
})