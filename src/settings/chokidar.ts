


export const chokidar_settings = {
    ignoreInitial:false,
    ignored:[/node_modules/i,/package.json/i,/package-lock.json/i,/^\./],
    awaitWriteFinish:{
        stabilityThreshold:1000
    }
}