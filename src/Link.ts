import { SharedModule, Root } from "./Share/ShareSettings";
import { join, isAbsolute, dirname } from "path";
import { shared_foldername } from "./location/Config";
import memoizee from "memoizee";
import EventEmitter from "events";
import mkdir from "mkdirp";

import {fromEvent} from "rxjs/observable/fromEvent";
import {merge} from "rxjs/observable/merge";
import {map} from "rxjs/operators/map";

import chokidar from "chokidar";
import { chokidar_settings } from "./settings/chokidar";
import { Stats, stat, createReadStream, createWriteStream } from "fs";
import { Relative } from "./location/Relative";
import { Observable } from "rxjs";
import { promisify } from "util";
import { ShouldTransfer } from "./Share/Negotiate";




interface UpdatedSharedModule{
    relative:Relative
    stats:Stats
}




export function LinkShare(shared:SharedModule,root:Root){

    let shared_folder = GetShareLocation(shared,root);

    let destination_watcher = onAddandChange(GetFolderWatcher(shared_folder),shared_folder);

    
    let source_watcher = onAddandChange(GetFolderWatcher(shared.source),shared.source);


    destination_watcher.subscribe(updated_file=>{
        //needs to go the the shared library folder
        let from = updated_file.relative.FullPath;
        let to = join(shared.source,updated_file.relative.relativePath);
        Transfer(from,to);
    })


    source_watcher.subscribe(updated_file=>{
        //Needs to go to the root project
    
        let from = updated_file.relative.FullPath;
        let destination = join(shared_folder,updated_file.relative.relativePath)
        Transfer(from,destination);
    })


    


    


    

    
    
    
}



function GetShareLocation(shared:SharedModule,root:Root){
    const shared_location = join(root,shared_foldername,shared.name);
    return shared_location;
}



const GetFolderWatcher = memoizee((folder:string)=>{

    return chokidar.watch(folder,chokidar_settings)

})

function onAddandChange(watcher,folder:string):Observable<UpdatedSharedModule>{
    let add = fromEvent(watcher,"add");
    let change = fromEvent(watcher,"change");


    return merge(add,change).pipe(map(([file,stats]:[string,Stats])=>{

        file = isAbsolute(file)?file:join(folder,file)

        let relative = new Relative(folder,file)

        let result =  {
            relative,stats
        } as UpdatedSharedModule


        return result;
    })) 
}

async function Transfer(from:string,to:string){

    let info = promisify(stat)
    
    let fromStats = await info(from);

    try{
        let toStats = await info(to);
    
        if(ShouldTransfer(fromStats,toStats)){
            createReadStream(from).pipe(createWriteStream(to))
        }

    }catch{
        await promisify(mkdir)(dirname(to))
        createReadStream(from).pipe(createWriteStream(to))
    }
}