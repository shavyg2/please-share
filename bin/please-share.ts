#!/usr/bin/env node
import meow from "meow";
import { Application } from "../src/Api";

const cli = meow(`
    Usage
    please-share ls

    List shared folders
`);

let application = Application.factory.Create();


;(async () => {
  if (
    cli.input.some((input: string) => {
      return input.toLowerCase() === "ls";
    })
  ) {
    let shares = await application.GetShares();

    let wait = shares.map(async share => {
      try {
        let name = await share.GetName();
        let src = share.GetSrc();
        let statement = `${name}: ${src}`;
        return [statement];
      } catch {
        let statement = ["Error printing share", share.GetSrc()];
      }
    });

    const statments = await Promise.all(wait);

    let flat_statments = statments.reduce(
      (statements_container, statment) => {
        return [...statements_container, ...statment];
      },
      ["Shares"]
    ) as string[];
    console.log(flat_statments.join("\n"));
    console.log("");
  } else {
    application.StartShare();
    
  }
})();
